//
//  Helper.h
//  Souq
//
//  Created by Basil Al Shaikh Hasan on 3/18/16.
//  Copyright © 2016 Basil Al Shaikh Hasan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>


@class Helper;

@interface Helper : NSObject
{
    UIActivityIndicatorView *ActivityIndicator;
    UIView *GrayBackView;
}


@property (nonatomic,retain) NSString *serverURL;


+(Helper   *)getHelperInstance;

+(BOOL)checkInternetConnection;

@end
