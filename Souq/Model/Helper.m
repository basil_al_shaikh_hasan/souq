//
//  Helper.m
//  Souq
//
//  Created by Basil Al Shaikh Hasan on 3/18/16.
//  Copyright © 2016 Basil Al Shaikh Hasan. All rights reserved.
//


#import "Helper.h"
#import "Reachability.h"

@interface Helper()

@end

static Helper *objHelper = nil;

@implementation Helper

@synthesize serverURL ;

//  ******************************
//  *****   Initialization   *****
//  ******************************
- (id)init
{
    self = [super init] ;
    
    self.serverURL = @"http://developer.souq.com";

    
    return self;
}

//  ************************************
//  *****   Get Helper Instance   *****
//  ************************************
+ (Helper *) getHelperInstance;	//		static method
{
    @synchronized (objHelper)
    {
        if ( !objHelper || objHelper == NULL )
        {
            objHelper = [[Helper alloc] init];
        }
        
        return objHelper;
    }
}


-(void) showGrayViewWithAnimation:(UIView *)grayV
{
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    
    [grayV setHidden:NO];
    [grayV setAlpha:0.0f];
    
    [UIView animateWithDuration:0.2f delay:0.1f options:UIViewAnimationOptionCurveEaseInOut animations:^
     {
         [grayV setAlpha:1.0f];
     }completion:^(BOOL finished){}];
}
-(void)HideGrayViewWithAnimation:(UIView *)grayV
{
    [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    
    [grayV setAlpha:1.0f];
    
    [UIView animateWithDuration:0.3f
                          delay:0.1f
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         [grayV setAlpha:0.0f];
                     }
                     completion:^(BOOL finished){
                         [grayV setHidden:YES];
                     }];
    
}

+(BOOL)checkInternetConnection
{
    Reachability* internetReachable;
    Reachability* hostReachable;
    
    bool internetActive;
//    bool hostActive;
    
    //========================check for internet connection=======================//
    internetReachable = [Reachability reachabilityForInternetConnection];
    hostReachable = [Reachability reachabilityWithHostName:objHelper.serverURL];
    
    // called after network status changes
    NetworkStatus internetStatus = [internetReachable currentReachabilityStatus];
    switch (internetStatus)
    {
        case NotReachable:
        {
            NSLog(@"The internet is down.");
            internetActive = NO;
            break;
        }
        case ReachableViaWiFi:
        {
            NSLog(@"The internet is working via WIFI.");
            internetActive = YES;
            break;
        }
        case ReachableViaWWAN:
        {
            NSLog(@"The internet is working via WWAN.");
            internetActive = YES;
            break;
        }
    }
    
    if (internetActive) {
        return YES;
    }
    else
    {
        return NO;
    }
}

@end
