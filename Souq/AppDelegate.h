//
//  AppDelegate.h
//  Souq
//
//  Created by Basil Al Shaikh Hasan on 3/18/16.
//  Copyright © 2016 Basil Al Shaikh Hasan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

