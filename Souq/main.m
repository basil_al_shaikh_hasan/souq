//
//  main.m
//  Souq
//
//  Created by Basil Al Shaikh Hasan on 3/18/16.
//  Copyright © 2016 Basil Al Shaikh Hasan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
