//
//  LoginViewController.m
//  Souq
//
//  Created by Basil Al Shaikh Hasan on 3/18/16.
//  Copyright © 2016 Basil Al Shaikh Hasan. All rights reserved.
//

#import "LoginViewController.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

@synthesize loginButtonOutlit , userNameTextField , passwordTextField;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    loginButtonOutlit.layer.cornerRadius = 4;
    loginButtonOutlit.layer.masksToBounds = YES;
    
    objHelper  = [Helper  getHelperInstance];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Text field handling
-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    if (textField == userNameTextField) {
        [passwordTextField becomeFirstResponder];
    }
    else
    {
        [textField resignFirstResponder];
    }
    return YES;
}

-(IBAction)loginButtonPressed:(id)sender
{
    if ([userNameTextField.text length] == 0) {
        [self createAlertViewWithTitle:@"" message:@"Please enter your Username"];
    }
    else if ([passwordTextField.text length] == 0) {
        [self createAlertViewWithTitle:@"" message:@"Please enter the Password"];
    }
    else if([Helper checkInternetConnection])
    {
        // To Do : call login webservice
        
        UIStoryboard * story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        ProductTypeViewController * controler = [story instantiateViewControllerWithIdentifier:@"ProductType"];
        [self.navigationController pushViewController:controler animated:YES];
    }
    else
    {
        [self createAlertViewWithTitle:@"No Internet Connection" message:@"Please check your internet connection"];
    }
}

-(void)createAlertViewWithTitle:(NSString *)title message:(NSString *)msg
{
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:title
                                  message:msg
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction
                                actionWithTitle:@"Ok"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
                                    [self dismissViewControllerAnimated:YES completion:nil];
                                }];
    
    [alert addAction:okButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
