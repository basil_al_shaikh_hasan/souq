//
//  LoginViewController.h
//  Souq
//
//  Created by Basil Al Shaikh Hasan on 3/18/16.
//  Copyright © 2016 Basil Al Shaikh Hasan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Helper.h"
#import "ProductTypeViewController.h"

@interface LoginViewController : UIViewController <UITextFieldDelegate>
{
    Helper * objHelper;

}

@property (retain, nonatomic) IBOutlet UIButton *loginButtonOutlit;
@property (retain, nonatomic) IBOutlet UITextField *userNameTextField;
@property (retain, nonatomic) IBOutlet UITextField *passwordTextField;

@end
