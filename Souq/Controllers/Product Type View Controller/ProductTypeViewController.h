//
//  ProductTypeViewController.h
//  Souq
//
//  Created by Basil Al Shaikh Hasan on 3/19/16.
//  Copyright © 2016 Basil Al Shaikh Hasan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Helper.h"
#import "ProductType_Class.h"
#import "productTypeTableViewCell.h"

@interface ProductTypeViewController : UIViewController <UITabBarControllerDelegate , UITableViewDataSource>
{
     Helper * objHelper;
    
    NSMutableArray * produceType;
    
    NSMutableArray * sellectedItems;
}

@property(strong)NSDictionary *productTypesDictionary;
@property (nonatomic, strong) IBOutlet UITableView * productTable;

@end
