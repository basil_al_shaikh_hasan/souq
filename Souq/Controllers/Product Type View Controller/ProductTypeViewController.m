//
//  ProductTypeViewController.m
//  Souq
//
//  Created by Basil Al Shaikh Hasan on 3/19/16.
//  Copyright © 2016 Basil Al Shaikh Hasan. All rights reserved.
//

#import "ProductTypeViewController.h"

@interface ProductTypeViewController ()

@end

@implementation ProductTypeViewController

@synthesize productTypesDictionary , productTable;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    objHelper  = [Helper  getHelperInstance];
    
    produceType = [[NSMutableArray alloc]init];
    sellectedItems = [[NSMutableArray alloc]init];
    
    if([Helper checkInternetConnection])
    {
        // To Do : call webservice that retrieve product types, but in this case I Assume the existence and retrieve The JSON and I will read it, this JSON data I stored it in the ProductTypeString.txt File
        produceType = [self productTypeParser];
        for(int i = 0 ; i< [produceType count] ; i++) {
            [sellectedItems addObject:@"0"];
        }
    }
    else
    {
        [self createAlertViewWithTitle:@"No Internet Connection" message:@"Please check your internet connection"];
    }
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


-(NSMutableArray *)productTypeParser{
    
    NSString *filePathCountries = [[NSBundle mainBundle] pathForResource:@"productTypes" ofType:@"txt"];
    NSData *myData = [NSData dataWithContentsOfFile:filePathCountries];
    
    NSError* __autoreleasing jsonParsingError = nil;
    
    productTypesDictionary= [NSJSONSerialization JSONObjectWithData:myData options:0 error: &jsonParsingError];
    
    NSMutableArray * allProducts = [[NSMutableArray alloc]init];
    
    ProductType_Class * product;
    
    for (id obj in productTypesDictionary) {
        product = [[ProductType_Class alloc]init];
        product.product_ID = [obj objectForKey:@"productID"];
        product.product_name = [obj objectForKey:@"productType"] ;
        [allProducts  addObject:product];
        
    }
    return allProducts;
}

-(void)createAlertViewWithTitle:(NSString *)title message:(NSString *)msg
{
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:title
                                  message:msg
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:@"Ok"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   [self dismissViewControllerAnimated:YES completion:nil];
                               }];
    
    [alert addAction:okButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return  [produceType count] ;
}

- (CGFloat)tableView:(UITableView *)_tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 65;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *simpleTableIdentifier = @"productTypeCell";
    
    productTypeTableViewCell *cell = (productTypeTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"productTypeTableViewCell" owner:self options:nil];
    cell = [nib objectAtIndex:0];
    
    
    
    ProductType_Class * product = [produceType objectAtIndex:indexPath.row];
    
    cell.productTypeLabel.text = product.product_name;
    
    if ([[sellectedItems objectAtIndex:indexPath.row] isEqualToString:@"0"]) {
        cell.backView.backgroundColor = [UIColor groupTableViewBackgroundColor];
        cell.arrowImage.hidden = YES;
        [cell.productTypeLabel setTextColor:[UIColor blackColor]];
    }
    else
    {
        cell.backView.backgroundColor = [UIColor colorWithRed:13.0/255.0f green:114.0/255.0f blue:254.0/255.0f alpha:1.0];
        cell.arrowImage.hidden = NO;
        [cell.productTypeLabel setTextColor:[UIColor whiteColor]];
    }
    //
    UIView *bgColorView = [[UIView alloc] init];
    [bgColorView setBackgroundColor:[UIColor clearColor]];
    [cell setSelectedBackgroundView:bgColorView];
    //
    //    NSArray *selectedRows = [tableView indexPathsForSelectedRows];
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([[sellectedItems objectAtIndex:indexPath.row] isEqualToString:@"0"]) {
    [sellectedItems removeObjectAtIndex:indexPath.row];
    [sellectedItems insertObject:@"1" atIndex:indexPath.row];
    }
    else
    {
        [sellectedItems removeObjectAtIndex:indexPath.row];
        [sellectedItems insertObject:@"0" atIndex:indexPath.row];
    }
    
    [productTable reloadData];
    
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {

}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end


