//
//  ProductType_Class.h
//  Souq
//
//  Created by Basil Al Shaikh Hasan on 3/19/16.
//  Copyright © 2016 Basil Al Shaikh Hasan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ProductType_Class : NSObject

@property (nonatomic , strong)NSString *product_ID;
@property (nonatomic , strong)NSString *product_name;

@end
