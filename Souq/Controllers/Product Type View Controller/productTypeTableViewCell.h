//
//  productTypeTableViewCell.h
//  Souq
//
//  Created by Basil Al Shaikh Hasan on 3/19/16.
//  Copyright © 2016 Basil Al Shaikh Hasan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface productTypeTableViewCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel * productTypeLabel;
@property (nonatomic, strong) IBOutlet UIView * backView;
@property (nonatomic, strong) IBOutlet UIImageView * arrowImage;

@end
